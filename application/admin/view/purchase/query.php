{extend name="base:base" /}
{block name="body"}  
<div class="table-common">
    <div class="search-box">
        <form class="form-inline" action="{:url('product_build_query')}" method="get">
            <input size="16" type="text" class="datetime_search form-control" name="timea" value="{$Think.get.timea}" placeholder="创建开始日期">
            <i class="fa fa-arrows-h"></i>
            <input size="16" type="text" class="datetime_search form-control" name="timeb" value="{$Think.get.timeb}" placeholder="创建结束日期">
            <input type="text" placeholder="识别码/产品名称" name="keyword" value="{$Think.get.keyword}" class="form-control" />
            <button type="submit" class="btn btn-primary " title="搜索"><i class="glyphicon glyphicon-search"></i> 搜索</button>
        </form>
    </div>
</div>
<p>
    <small><i class="iconfont icon-tishi"></i> 查询到了<strong>{$count}</strong>个采购记录</small>
</p>
<?php if (isset($lists) && count($lists) > 0) { ?>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th style="width:20px"></th>
                <th>编号</th>
                <th>状态</th>
                <th>采购日期</th>
                <th>数量</th>
                <th>操作人</th>        
                <th>备注</th>
                <th style="text-align:center">撤消</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($lists as $key => $var) {
                ?>
            <tr class="{$var.status=='-1'?' warning':($var.status=='-2'?' success':'')}" >
                    <td class="product_dataplus" id="product_dataplus{$var.id}"  data-id="{$var.id}" style="cursor: pointer"><i class="glyphicon glyphicon-menu-right"></i></td>
                    <td class="product_dataplus" id="product_dataplus{$var.id}"  data-id="{$var.id}">{$var.order_number}</td>
                    <td>{$var.status_text}</td>
                    <td>{$var.create_time}</td>
                    <td>{$var.total_quantity}</td>
                    <td>{$var.staff_nickname}</td>
                    <td title="{$var.remark}">{$var.remark}</td>
                    <td style="text-align:center">
                        <a href="{:url('query_delete',['id'=>$var.id])}" class="btn btn-warning btn-xs ajax-get confirm" title="相关的入库订单会同步删除，确认执行此操作吗？" >撤消</a>
                    </td>
                </tr>
                <tr id="product_data{$var.id}" class="product_data" style="display:none">
                    <td colspan="8">
                        <table class="table table-hover table-striped table-bordered" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>图片</th>
                                    <th>产品名称</th>
                                    <th>成本价</th>
                                    <th>采购价</th>
                                    <th>数量</th>                                    
                                    <th>小计</th>                                    
                                    <th>已入库</th>                                    
                                    <th>供应商</th>
                                    <th>入库</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($var['order_data'])) {
                                    foreach ($var['order_data'] as $key2 => $var2) {
                                        
                                        // 产品快照
                                        $product = unserialize($var2['product_snapshot']);
                                        
                                        ?>
                                        <tr>
                                            <td>{:sprintf("%06d",$var2.id)}</td>
                                            <td><img src="<?php echo img_resize($product['image'], 400, 400) ?>" style="max-width: 100px; max-height: 100px;" class="img-thumbnail" /></td>
                                            <td>{$product.name}</td>
                                            <td>{$product.purchase}</td>
                                            <td>{$var2.group_price}</td>
                                            <td>{$var2.quantity}</td>                                           
                                            <td>{$var2.amount}</td>                                           
                                            <td>{$var2.puts}</td>                                           
                                            <td>{$var2.supplier_text}</td>
                                            <td>
                                                {if $var2.status>-2}
                                            <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal" href="{:url('storage',['id'=>$var2.id])}" data-title="产品入库" title="入库">入库</a>
                                                {/if}
                                                
                                                {if $var2.status<0}
                                            <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal" href="{:url('storage_history',['id'=>$var2.id])}" data-title="入库记录" title="入库记录">入库记录</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <p class="bg-warning center-block">   
        <i class="iconfont icon-wushuju"></i> 暂时没有相关数据
    </p>
<?php } ?>
{$pages}
{/block}
{block name="foot_js"} 
<script type="text/javascript">
    
     
    
    
    // 展开列表
    $('.product_dataplus').click(function(){
        
        var id = $(this).data('id');
        $('#product_data' + id).toggle(10, function () {

            if ($(this).is(":hidden")) {
                $('#product_dataplus' + id).html('<i class=\'glyphicon glyphicon-menu-right\'></i>');
            } else {
                $('#product_dataplus' + id).html('<i class=\'glyphicon glyphicon-menu-down\'></i>');
            }
            return false;

        });
        
    });
</script>
{/block}