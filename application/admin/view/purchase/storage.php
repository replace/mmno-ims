<div class="panel panel-default">
    <div class="panel-heading">采购入库</div>
    <div class="panel-body">
        
        <?php 
        $product_snapshot = unserialize($order_data['product_snapshot']);
        ?>
        
        <form class="form-inline sales_returns_add" action="{:url('storage',['id'=>$order_data.id])}" method="post">
          
            <table class="table table-hover">
                <tr>
                    <th style="width:130px;text-align:right">产品识别码</th>
                    <td>{$product_snapshot.code}</td>
                </tr>
                <tr>
                    <th style="text-align:right">产品名称</th>
                    <td>{$product_snapshot.name}</td>
                </tr>
                <tr>
                    <th style="line-height:30px;text-align:right">入库仓库</th>
                    <td>
                        <select name="warehouse" class="form-control">
                            <option value="">所有仓库</option>
                            {volist name="warehouse" id="var"}
                            <option value="{$var.id}"{$var.default==1?' selected':''}>{$var.name}</option>
                            {/volist}
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align:right">入库价</th>
                    <td><input class="form-control" name="group_price" value="{$order_data.group_price}" /></td>
                </tr>
                <tr>
                    <th style="width:130px;line-height:30px;text-align:right">入货数量</th>
                    <td>
                        <div class="input-prepend input-append">
                            <button type="button" class="btn btn-default" onclick="$('#quantityquantity').val((Number($('#quantityquantity').val()) - 1) < 1 ? 1 : (Number($('#quantityquantity').val()) - 1))"><i class="glyphicon glyphicon-minus"></i></button>
                            <input type="text" id="quantityquantity"  name="quantity" value="{$order_data.quantity-$order_data.puts}" placeholder="数量" class="form-control">
                            <button type="button" class="btn btn-default" onclick="$('#quantityquantity').val((Number($('#quantityquantity').val()) + 1) > {$order_data.quantity - $order_data.puts} ? $('#quantityquantity').val() : (Number($('#quantityquantity').val()) + 1))"><i class="glyphicon glyphicon-plus"></i></button>
                        </div>
                        <p class="help-block">可入货数量为:{$order_data.quantity-$order_data.puts}</p>
                    </td>
                </tr>     
                <tr>
                    <th style="text-align:right">供应商</th>
                    <td>{$order_data.supplier_text}</td>
                </tr>
                <tr>
                    <th style="line-height:30px;text-align:right">备注</th>
                    <td><textarea name="remark" type="" class="form-control" style="height:50px"></textarea></td>
                </tr>
                <tr>
                    <th style="width:130px;line-height:30px;text-align:right"></th>
                    <td><button type="submit" class="btn btn-primary ajax-post" target-form="sales_returns_add"><i class="glyphicon glyphicon-floppy-disk"></i> 保存</button> </td>
                </tr>
            </table>
        </form>
    </div>
</div>