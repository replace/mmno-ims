<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TODO supply a title</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    <body style="font-size: 12px">
        <div><br><br><br>
            <table width="90%" border="0" cellpadding="0" cellspacing="5" align="center" >

<!--经办： {$info.staff_nickname}
  打印： {$user_info.nickname}-->

                <tr>
                    <td align="left" style="width:50%;"> 
                        
                        订单编号：{$info.order_number}
                        
<!--                        客户： <span style="text-decoration:underline;"> {$info.nickname? $info.nickname : '无' } </span>-->
                    </td>
                    <td align="left" style="width:50%;"> 
                        
                    </td>
                </tr>


                <tr>
                    <td align="left"> 
                        客户姓名：{$info.express_people}
                    </td>
                    <td align="left"> 
                        出库日期： {:date('Y-m-d H:i:s', $info.ship_time)}
                    </td>
                </tr>
               <tr>
                    <td align="left"> 
                      联系电话：{$info.express_phone}
                    </td>
                    <td align="left"> 
                        打印日期： {:date('Y-m-d')}
                    </td>
                </tr>

                <tr style="display: none;">
                     <td align="left"> 
                         物流信息：<span style="text-decoration:underline;"> {$info.express_name} </span>
                    </td>
                    <td align="left"> 
                       单号：<span style="text-decoration:underline;"> {$info.express_num} </span>
                    </td>
                     
                </tr>
                
                <tr>
                    <td align="left" colspan="2">

                        收货地址：<span style="text-decoration:underline;">{$info.express_addr}</span>


                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">

                        客服备注：{$info.remark}

                    </td>
                </tr>
                <tr >
                    <td colspan="2">
<!--                         border="1" cellspacing="0" cellpadding="2" align="center" bordercolor="#000000"   -->
                        <table width="100%" cellpadding="0" cellspacing="1" bgcolor="black">
                            <tr bgcolor="white">
                                <th height="30">货号</th>
                                 <th>产品名称</th>
<!--                                <th>规格</th>-->
                                <th>单价</th>
                                <th>数量</th>
                                <th>金额</th>
                                <th>备注</th>

                            </tr>
                            <?php
                            $quantity = 0;
                            $amount = 0;
                            $sales = 0;
                            ?>
                            <?php
                            foreach ($orders as $key => $var) {
                                $var['product_data'] = unserialize($var['product_data']);

                                $units = db('product_unit')->column('id,name');

                               
                                 ?>
                                <?php
                                $quantity += $var['quantity'];
                                $amount += $var['amount'];
                                $sales += $var['product_data']['sales'];
                                ?>
                                <tr bgcolor="white">
                                    <td height="25" align="center">{$var.product_data.code}</td>
                                     <td align="center">{$var.product_data.name}</td>
<!--                                    <td align="center">{$var.product_data.format}</td>-->
                                    <td align="center">
                                        <?php echo number_format($var['group_price'], 2); ?>
                                    </td>
                                    <td align="center">{$var.quantity}</td>
                                    
                                    <td align="center"> <?php echo number_format($var['amount'], 2); ?> </td>
                                    <td align="center">{$var.remarks}</td>
                                </tr>
                            <?php } ?>
                            <tr bgcolor="white">
                                <th align="center" height="30">合计</th>
                                 <th align="center"></th>
<!--                                <td align="center"></td>-->
                                <th align="center">&nbsp;</th>
                                <th align="center">{$quantity}</th>
                                <th align="center"> <?php echo number_format($amount, 2); ?> </th>
                                 <th align="center"></th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <div style="display: flex; flex-flow: column;">
                            <div style="height: 30px"></div>
                             
                             <div>作为您的退换货凭证，请妥善保存此单据<br>退换货流程详见单据反面</div>
                             
<!--                             <img src="__PUBLIC__/static/admin/images/vna.jpg" width="120" height="120">
                        
                             为纳百川公众号
                             <div>www.vna.hk</div>-->
                        </div>
                       


                    </td>
                </tr>

            </table>
        </div>
    </body>
</html>
